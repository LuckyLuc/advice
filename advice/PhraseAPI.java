import android.util.Log;
import java.util.Random;

/**
 * Created by Phil.Dahlheimer on 2/7/2015.
 */
 
 
// the bulk of the logic happens here
public class PhraseAPI 
{
    // standard Java Random
    private Random r = new Random();
    
    // used for phrase building
    // what words should we use?
    Dictionary D; 
    
    // what phrases should I use?
    Grammar G;

    // using the grammar and dictionary inputs, return a random phrase
    // PhraseType { 1 = advice }
    // Dictionary { 1 = default, 2 = children, 3 = seasonal? (date-driven) }
    public PhraseResponse GetRandomPhrase(int phraseType, int dictionaryEdition)
    {
        // sentence is an array of words
        W[] sentence;
        
        // return var
        PhraseResponse pr = new PhraseResponse();
        
        // set dictionary and grammar with API inputs
        D = new Dictionary(dictionaryEdition);
        G = new Grammar(phraseType);
        
        // TODO: move this type check into Grammar init
        if (phraseType == 1)
        {
            // advice
            // get an array of word enums
            sentence = GetRandomSentence(G.sentences);
            
            // that happened
            pr.Success = true;
            
            // parse the word enum array into a readable sentence
            pr.Phrase = ParseSentence(sentence);
        } 
        else 
        {
            pr.ErrorMessage = "This phrase request is not yet supported.";
            pr.Success = false;
        }
        
        // send phrase response
        return pr;
    }

    // pick a sentence from the grammar sentences provided
    // TODO: pass by ref
    private W[] GetRandomSentence (W[][] sentenceArray)
    {
        W[] sentence = sentenceArray[GetRandomNumber(0, sentenceArray.length-1)];

        return sentence;
    }

    // translate an array of W enums to an English sentence
    private String ParseSentence(W[] sentence)
    {
        // return value
        String parsedSentence = "";
        
        // exclusion var
        String parsedWords = "";
        
        // vowels for testing indefinite article 
        char[] vowels = {'a', 'e', 'i', 'o', 'u'};

        // for making the sentence more grammatically pleasing
        boolean addArticle = false;
        boolean addArticleNext = false;
        boolean addComma = false;

        // loop the W array
        for(int i = 0; i < sentence.length; i++)
        {
            // loop var
            String parsedWord;
            
            // did the last W tell us to ad an indefinite article to this W?
            if (addArticleNext)
            {
                addArticle = true;
            }
            else
            {
                addArticle = false;
            }
            // set back to false
            addArticleNext = false;
            
            // switch statement for handling W types
            switch (sentence[i]) 
            {
                // if it's indefinitel article, we don't know what to add until the next word is selected
                // this will never be the last word unless there's a problem
                case ARTICLE_INDEFINITE:
                    // add replace token for article
                    parsedWord = "%%ARTICLE%%";
                    addArticleNext = true;
                    break;
                // add a comma
                case COMMA:
                    // add replace token for comma
                    parsedWord = "%%COMMA%%";
                    addComma = true;
                    break;
                // a number that might be just one
                case NUMBER:
                    parsedWord = Integer.toString(GetRandomNumber(1, 10));
                    break;
                // a definitely plural number
                case NUMBERS:
                    parsedWord = Integer.toString(GetRandomNumber(2, 10));
                    break;
                // it's not a special case. do exact or type matching
                default:
                    // parse it, do not use the same word twice
                    parsedWord = ParseWord(sentence[i], parsedWords);
                    
                    // add it to parsed words string, pipe delimited - used for exclusions - string for ease of using contains()
                    parsedWords+= parsedWord + "||";
                    break;
            }

            // add an article? check if the first letter is a vowel
            // of course this isn't perfect, but control over the dictionary helps for now
            // TODO: robustify the article check
            if (addArticle)
            {
                String article = "a";
                for (int j = 0; j < vowels.length; j++)
                {
                    // TODO: use ListContains
                    if (vowels[j] == parsedWord.charAt(0))
                    {
                        // first letter is a vowel
                        article = "an";
                        
                        // exit loop
                        break;
                    }
                }
                
                // add article to word
                parsedWord = article + " " + parsedWord;
            }

            // Capitalize first word if this is first loop or it's the second loop and we added an article
            if (i == 0 || (i == 1 && addArticle))
            {
                parsedWord = Capitalize(parsedWord);
            }
            
            // take off any white space and add a comma
            if (addComma)
            {
                parsedSentence = parsedSentence.trim();
                parsedWord = ",";
            }
            
            // we're not adding an article next loop, so add this parsed word to the sentence
            if (!addArticleNext)
            {
                parsedSentence += parsedWord;
                
                // add a space if it's not the last word
                if (i != (sentence.length-1)) 
                {
                    parsedSentence += " ";
                }
            }
            
            // back to false with you
            addArticle = false;
            addComma = false;
        }
        
        // no questions
        // TODO: add ending punctuation for questions when they exist in Grammar
        parsedSentence += ".";
        
        return parsedSentence;
    }

    // get a random word of type W that isn't in exclusions
    private String ParseWord(W word, String exclusions)
    {
        // return var
        String parsedWord;

        switch (word) 
        {
            // switch on type
            case ADJECTIVE:  parsedWord = GetRandomStringFromArray(D.adjective, exclusions);
                break;
            case ADVERB:  parsedWord = GetRandomStringFromArray(D.adverb, exclusions);
                break;
            case ARTICLES_INDEFINITE:  parsedWord = GetRandomStringFromArray(D.articles, exclusions);
                break;
            case CONCEPT:  parsedWord = GetRandomStringFromArray(D.concept, exclusions);
                break;
            case CONCEPTS:  parsedWord = GetRandomStringFromArray(D.concepts, exclusions);
                break;
            case FREQUENCY:  parsedWord = GetRandomStringFromArray(D.frequency, exclusions);
                break;
            case FREQUENCY_VERBOSE:  parsedWord = GetRandomStringFromArray(D.frequencyVerbose, exclusions);
                break;
            case PEOPLE:  parsedWord = GetRandomStringFromArray(D.people, exclusions);
                break;
            case PERSON:  parsedWord = GetRandomStringFromArray(D.person, exclusions);
                break;
            case PLACE:  parsedWord = GetRandomStringFromArray(D.place, exclusions);
                break;
            case PLACES:  parsedWord = GetRandomStringFromArray(D.places, exclusions);
                break;
            case PREPOSITION:  parsedWord = GetRandomStringFromArray(D.preposition, exclusions);
                break;
            case THING:  parsedWord = GetRandomStringFromArray(D.thing, exclusions);
                break;
            case THINGS:  parsedWord = GetRandomStringFromArray(D.things, exclusions);
                break;
            case TIME:  parsedWord = GetRandomStringFromArray(D.time, exclusions);
                break;
            case VERB_INTRANS:  parsedWord = GetRandomStringFromArray(D.verbIntransitive, exclusions);
                break;
            case VERB_TRANS:  parsedWord = GetRandomStringFromArray(D.verbTransitive, exclusions);
                break;
            case VERB_INTRANS_PAST:  parsedWord = GetRandomStringFromArray(D.verbIntransitivePast, exclusions);
                break;
            case VERB_TRANS_PAST:  parsedWord = GetRandomStringFromArray(D.verbTransitivePast, exclusions);
                break;
            // word is not a type (or type is not supported); use ToString to replace exact
            default: parsedWord = word.toString();
                break;
        }

        return parsedWord;
    }
    
    // get a random string from an array of strings 
    private String GetRandomStringFromArray (String[] sourceArray, String exclusions)
    {
        // pick one
        String randomString = sourceArray[GetRandomNumber(0, sourceArray.length-1)];

        // check it; if the word is excluded, pick another
        // TODO: infinite recursion warning! 
        //     Only if a sentence ever has more of a single type of word than there are entires of that type in the dictionary
        // TODO: for performance of retry, add excluded numbers to GetRandom to reduce trips of accidentally randoming the same number
        if (exclusions.contains(randomString))
        {
            // call self again
            randomString = GetRandomStringFromArray(sourceArray, exclusions);
        }

        return randomString;
    }

    // enbiggenate the first char of word. will not break already embiggened words
    private String Capitalize(String word)
    {
        return Character.toUpperCase(word.charAt(0)) + word.substring(1);
    }

    // get a random number between min and max
    // TODO: add exclusions (see GetRandomStringFromArray)
    private int GetRandomNumber(int min, int max)
    {
        return r.nextInt(max - min + 1) + min;
    }

    // is the string in the array?
    private boolean ListContains(String[] array, String string)
    {
        for(int i = 0; i<array.length; i++)
        {
            if (array[i] == string)
            {
                return true;
            }
        }
        return false;
    }

    private boolean IsBlacklist(String word)
    {
        return ListContains(D.blacklist, word);
    }

    private boolean IsUnfit(String word, int edition)
    {
        return ListContains(D.unfit, word);
    }

    private boolean IsInModeration(String word, int edition)
    {
        return ListContains(D.moderation, word);
    }

    private boolean IsDuplicate(String word, int edition)
    {
        return ListContains(D.duplicate, word);
    }
}
