/**
 * Created by Phil.Dahlheimer on 2/6/2015.
 */

// enum for different types of words
// these can be used for word types (parts of speech)
// exact words/phrases (these are used to maintain the sound/tone of the phase desired)
enum W 
{
    // types
    THING("thing"), // car, puppy, phone
    THINGS("things"), // carrots, lanterns
    PERSON("person"), // astronaut, football player
    PEOPLE("people"), // children, parents
    PLACE("place"), // Baltimore, bathroom
    PLACES("places"), // hotels, State Parks
    CONCEPT("concept"), // honor, duty, guilt
    CONCEPTS("concepts"), // oaths, promises
    ARTICLE_INDEFINITE("article-indefinite"), // a, an
    ARTICLES_INDEFINITE("article-definite"), // some, many, a few
    VERB_TRANS("verb-transitive"), // kick, slap "John, did you _______ Mommy?"
    VERB_INTRANS("verb-intransitive"), // sing, play, run "John, did you _______ with Mommy?"
    VERB_TRANS_PAST("verb-past-transitive"), // hugged, bit "John ______ his puppy on the playground"
    VERB_INTRANS_PAST("verb-past-intransitive"), // jumped, dreamed "John ________ with his puppy on the playground"
    PREPOSITION("preposition"), // in, on, around
    TIME("time"), // before, when, if, as
    ADVERB("adverb"), // quietly, quickly
    ADJECTIVE("adjective"), // thrown, better, chewy
    FREQUENCY("frequency"), // always, never, occasionally
    FREQUENCY_VERBOSE("frequency-verbose"), // by the end of the month
    
    // punctuation, special replacements
    COMMA(","),
    NUMBER("number"),
    NUMBERS("numbers"),
    
    // exact words/phrases
    DONT("don't"),
    IF_YOU_CANT("if you can't"),
    IN_THE("in the"),
    IS("is"),
    IS_BETTER_THAN("is better than"),
    IS_WORTH("is worth"),
    IT("it"),
    OR_YOULL_NEED("or you'll need"),
    SHOULD("should"),
    THAT("that"),
    THE("the"),
    THEIR("their"),
    THEM("them"),
    YOU("you"),
    YOU_CAN("you can"),
    YOUR("your")
    ;
    
    // used with ToString() override to print string value
    private String stringValue;
    private W(String toString) 
    {
        stringValue = toString;
    }

    @Override
    public String toString() {
        return stringValue;
    }
}