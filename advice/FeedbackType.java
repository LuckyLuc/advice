//package 

/**
 * Created by Phil.Dahlheimer on 2/7/2015.
 */
public enum FeedbackType {
    CONCERN("voice a concern"),
    ERROR("report an error"),
    COMPLIMENT("pay a compliment"),
    QUESTION("ask a question"),
    SUGGESTION("make a suggestion");

    private String stringValue;
    private FeedbackType(String toString) {
        stringValue = toString;
    }

    @Override
    public String toString() {
        return stringValue;
    }
}
