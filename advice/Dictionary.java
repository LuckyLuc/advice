/**
 * Created by Phil.Dahlheimer on 2/7/2015.
 */
 
// the collections of words available
public class Dictionary 
{
    // word arrays
    public String[] thing;
    public String[] things;
    public String[] person;
    public String[] people;
    public String[] verbTransitivePast;
    public String[] verbTransitive;
    public String[] verbIntransitivePast;
    public String[] verbIntransitive;
    public String[] preposition;
    public String[] time;
    public String[] adverb;
    public String[] adjective;
    public String[] frequency;
    public String[] frequencyVerbose;
    public String[] articles;
    public String[] concept;
    public String[] concepts;
    public String[] place;
    public String[] places;

    Dictionary(int edition)
    {
        // TODO: add editions/words
        if (edition == 1)
        {
            // default dictionary
            thing = new String[] { "puppy", "toilet", "spare tire", "cardigan", "mailbox", "baby", "candle", "couch", "computer", "shovel", "seashell", "tissue", "shopping cart", "hat", "hammer", "fork" };
            things = new String[] { "kitties", "movies", "lasers", "pants", "cars", "noodles", "crayons", "toys", "socks", "houses", "stairs", "teeth", "bottles", "groundhogs", "ceiling fans",
                    "chocolate bars", "pizza toppings", "candy stores", "puppets", "cell phones", "dice", "shoes", "tools" };
            person = new String[] { "astronaut", "man", "woman", "parent", "weather man", "teacher", "zombie", "scientist", "game show host", "car insurance salesman", "professional bowler", "brain donor" };
            people = new String[] { "girls", "boys", "children", "parents", "skydivers", "voice actors", "pilots", "rappers", "hipsters", "Facebook users", "time travelers", "robots", "criminals",
                    "politicians", "fat guys", "dentists", "cops", "carnies" };
            verbTransitivePast = new String[] { "punched", "pushed", "slapped", "murdered", "kissed", "tossed", "loved", "washed", "carried", "chewed", "stacked", "solved" };
            verbTransitive = new String[] { "trust", "cauterize", "break", "poke", "scratch", "feel", "flush", "carry", "swallow", "gargle", "chew", "eat", "juggle", "squeeze", "touch", "slap", "burn" };
            verbIntransitivePast = new String[] { "jumped", "sang" };
            verbIntransitive = new String[] { "sing", "work", "play", "run", "jump", "spit" };
            preposition = new String[] { "around", "behind", "on", "under", "in", "near", "through", "with", "for", "at", "above" };
            time = new String[] { "before", "when", "if", "after" };
            adverb = new String[] { "violently", "passionately", "wistfully", "cheerfully", "mournfully", "carefully", "terribly", "uncomfortably", "gratefully", "carelessly", "incessantly" };
            adjective = new String[] { "awkward", "pretty", "big", "fast", "sterile", "impressive", "sweet", "rough", "chubby", "silly", "hot", "cold", "slow", "wet", "salty", "chewy", "fun", "hard", "tender" };
            frequency = new String[] { "always", "never",  "never", "occasionally" };
            frequencyVerbose = new String[] { "every once in a while,", "in the name of decency,", "after every meal,", "before retirement", "next year", "as soon as possible", "if you haven't already" };
            articles = new String[] { "some", "many", "several", "a few", "most" };
            concept = new String[] { "liberty", "trust", "hope", "love", "honor", "decency", "malice", "lust", "greed", "envy", "anger", "gluttony" };
            concepts = new String[] { "hopes", "dreams", "fantasies", "nightmares"  };
            place = new String[] { "bathroom", "shower", "office", "mini mall", "flea market", "park", "road", "car", "city", "prison", "stadium" };
            places = new String[] { "lakes", "towns", "stores", "vars", "next year", "schools", "jails" };
        }
        else if (edition == 2){
            // children's dictionary
            // the goal for the children's dictionary is to add recognizable and silly words for children 
            // and, probably more importantly, words that when added together do not sound violent or innuendo-ish
            thing = new String[] { "puppy", "potty", "baby", "doll", "house", "blanket", "kitty", "chair", "apple", "banana", "car", "phone", "diaper", "hat", "shirt", "spoon", "plane", "spice" };
            things = new String[] { "blocks", "shoes", "pants", "socks", "noodles", "crayons", "toys", "houses", "stairs", "teeth", "bottles", "puppets" };
            person = new String[] { "daddy", "man", "woman", "parent", "mommy", "teacher", "stranger", "bear", "puppy", "kitty", "bus driver", "fireman", "princess" };
            people = new String[] { "girls", "boys", "children", "parents", "bunnies", "penguins", "raccoons", "robots", "knights", "princes" };
            verbTransitivePast = new String[] { "kissed", "tossed", "loved", "washed", "carried", "chewed", "stacked", "solved" };
            verbTransitive = new String[] { "trust", "break", "poke", "scratch", "feel", "flush", "carry", "swallow", "chew", "eat", "juggle", "squeeze", "slap" };
            verbIntransitivePast = new String[] { "jumped", "sang", "smiled", "danced", "played" };
            verbIntransitive = new String[] { "sing", "work", "play", "run", "jump", "smile", "dance" };
            preposition = new String[] { "around", "behind", "on", "under", "in", "with", "above" };
            time = new String[] { "before", "when", "if", "after" };
            adverb = new String[] { "cheerfully", "happily", "carefully" };
            adjective = new String[] { "pretty", "big", "fast", "sweet", "rough", "salty", "silly", "hot", "cold", "slow", "wet", "chewy", "fun", "hard" };
            frequency = new String[] { "always", "never" };
            frequencyVerbose = new String[] { "after every meal,", "before bed time", "next year", "when you start school", "if you haven't already" };
            articles = new String[] { "some", "many", "a few", "most" };
            concept = new String[] { "trust", "hope", "love", "honor", "decency", "greed", "envy", "anger", "gluttony" };
            concepts = new String[] { "hopes", "dreams", "fantasies", "nightmares"  };
            place = new String[] { "bathroom", "shower", "mall", "market", "park", "road", "town" };
            places = new String[] { "towns", "stores", "schools", "houses" };
        }
    }
}