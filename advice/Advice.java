//package name

import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class Advice extends ActionBarActivity {
    // TODO: suggest a word
    // TODO: unique user identification
    // TODO: log all gets tied to a userid
    // TODO: allow feedback
    // TODO: create a management App to handle suggestions/dictionary reports/feedback?
    // TODO: message on appstart: suggestions accepted, feedback reply, etc?
    // TODO: gamification. dictionary unlocks, # accepted suggestions, # tweeted, leaderboards
    // TODO: profile? unlocks follow you?
    // TODO: cap suggestions/day?
    // TODO: superuser all access id?
    // TODO: get this on a t-shirt
    // TODO: link to other apps from "about"
    // TODO: add free API caller to website
    // TODO: stats on site: # words, added, suggested, existing, most suggested, recently suggested (moderate it), etc

    // local vars
    private ProgressBar spinner;
    private TextView randomPhrase;
    private int userId = 1;
    private PhraseAPI papi = new PhraseAPI();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advice);

        spinner = (ProgressBar)findViewById(R.id.pbSpinner);
        spinner.setVisibility(View.GONE);
        randomPhrase = (TextView)findViewById(R.id.tvAdvice);
        randomPhrase.setVisibility(View.GONE);

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_advice, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void GetAdvice(View view){

        Log.d("GetAdvice", "clicked");
        view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        randomPhrase.setVisibility(View.GONE);
        spinner.setVisibility(View.VISIBLE);
        Log.d("GetAdvice", "GetRandomPhrase");
        PhraseResponse advice = papi.GetRandomPhrase(1, 2, userId);
        if(advice.Success){
            Log.d("GetAdvice", "setText: " + advice.Phrase);
            randomPhrase.setText(advice.Phrase);
        }else{
            Log.d("GetAdvice", "setText: " + advice.ErrorMessage);
            randomPhrase.setText(advice.ErrorMessage);
        }
        spinner.setVisibility(View.GONE);
        randomPhrase.setVisibility(View.VISIBLE);
    }

    public void GetAdviceOld(View view){

        Log.d("GetAdvice", "clicked");

        randomPhrase.setVisibility(View.GONE);
        spinner.setVisibility(View.VISIBLE);

        // Execute some code after 1 second have passed
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                spinner.setVisibility(View.GONE);
                Log.d("GetAdvice", "GetRandomPhrase");
                PhraseResponse advice = papi.GetRandomPhrase(1, 1, userId);
                if(advice.Success){
                    Log.d("GetAdvice", "setText: " + advice.Phrase);
                    randomPhrase.setText(advice.Phrase);
                }else{
                    Log.d("GetAdvice", "setText: " + advice.ErrorMessage);
                    randomPhrase.setText(advice.ErrorMessage);
                }
                randomPhrase.setVisibility(View.VISIBLE);
            }
        }, 200);
    }
}