/**
 * Created by Phil.Dahlheimer on 2/7/2015.
 */
 
public class Grammar 
{
    // array of sentences (which are arrays of words)
    public W[][] sentences;

    // edition = phrase type (1 = advice)
    Grammar(int edition)
    {
        //  TODO: add phrase types/sentence variety
        if (edition == 1)
        {
            // advice
            // if you can't beat them, join them
            W[] advice0 = { W.IF_YOU_CANT, W.VERB_TRANS, W.THEM, W.COMMA, W.VERB_TRANS, W.THEM };
            
            // never play with fire
            W[] advice1 = { W.FREQUENCY, W.VERB_INTRANS, W.PREPOSITION, W.THINGS };
            
            // never go outside with wet hair
            W[] advice2 = { W.FREQUENCY, W.VERB_INTRANS, W.PREPOSITION, W.ADJECTIVE, W.THINGS };
            
            // children should always respect their elders
            W[] advice3 = { W.PEOPLE, W.SHOULD, W.FREQUENCY, W.VERB_TRANS, W.THEIR, W.THINGS };
            
            // never trust robots
            W[] advice4 = { W.FREQUENCY, W.VERB_TRANS, W.PEOPLE };
            
            // brush your teeth after every meal or you'll need dentures
            W[] advice5 = { W.VERB_TRANS, W.YOUR, W.THINGS, W.FREQUENCY_VERBOSE, W.OR_YOULL_NEED, W.ARTICLES_INDEFINITE, W.THINGS };
            
            // a lie told passionately is worth 2 boring facts
            W[] advice6 = { W.ARTICLE_INDEFINITE, W.THING, W.VERB_TRANS_PAST, W.ADVERB, W.IS_BETTER_THAN, W.NUMBERS, W.ADJECTIVE, W.THINGS };
            
            // a bird in the hand is worth 2 in the bush
            W[] advice7 = { W.ARTICLE_INDEFINITE, W.THING, W.IN_THE, W.THING, W.IS_WORTH, W.NUMBER, W.IN_THE, W.THING };
            
            // a penny saved is a penny earned
            W[] advice8 = { W.ARTICLE_INDEFINITE, W.THING, W.VERB_TRANS_PAST, W.IS, W.ARTICLE_INDEFINITE, W.THING, W.VERB_TRANS_PAST };
            
            // you can always trust a police woman (in a children's movie)
            W[] advice9 = { W.YOU_CAN, W.FREQUENCY, W.VERB_TRANS, W.ARTICLE_INDEFINITE, W.PERSON };
            
            // when you come to a fork in the road, take it (Berra)
            W[] advice10 = { W.TIME, W.YOU, W.VERB_INTRANS, W.PREPOSITION, W.ARTICLE_INDEFINITE, W.THING, W.PREPOSITION, W.THE, W.PLACE, W.COMMA, W.VERB_TRANS, W.IT };
            
            // don't fake the funk on a nasty dunk (Shaq)
            W[] advice11 = { W.DONT, W.VERB_TRANS, W.THE, W.THING, W.PREPOSITION, W.ARTICLE_INDEFINITE, W.THING };

            // what are the sentences supported by this Grammar edition?
            sentences = new W[][]{ advice0, advice1, advice2, advice3, advice4, advice5, advice6, advice7, advice8, advice9, advice10, advice11 };
        }
    }
}